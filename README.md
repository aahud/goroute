# goroute

First things first, to use this tool you need an OpenRouteService API key.
Those API keys are available freely for individual use. You can acquire yours
from their [sign-up](https://openrouteservice.org/dev/#/signup) page.

### Purpose
This tool is intended to easily provide textual turn-by-turn directions.
Goroute is mostly just a wrapper for the OpenRouteService geocoding and routing
APIs.

### Sample(s)
```
> goroute -h
usage: goroute [-h|--help] [-l|--language "<value>"] [-s|--style
               (fastest|shortest|recommended)] [-u|--units (m|km|mi)]
               [-w|--walking] [-p|--point "<value>" [-p|--point "<value>" ...]]

               CLI turn-by-turn directions

Arguments:

  -h  --help      Print help information
  -l  --language  Instructions language. Two letter abbreviation. Default: en
  -s  --style     Choice of routing style. Default: fastest
  -u  --units     Units for distance metrics. Default: mi
  -w  --walking   Specify walking rather than driving instructions.
  -p  --point     Points or locations for which you want routes
```
```
> goroute -u km -p 'washington monument d.c.' -p 'lincoln memorial d.c.'
    2.627000 km to go, w/ eta 3.10 minutes

15th Street Northwest: Head southeast on 15th Street Northwest in 0.360 km
Independence Avenue Southwest: Turn right onto Independence Avenue Southwest in 1.628 km
Ohio Drive Southwest: Continue straight onto Ohio Drive Southwest in 0.311 km
-: Keep right in 0.218 km
Lincoln Memorial Circle Northwest: Turn sharp left onto Lincoln Memorial Circle Northwest in 0.046 km
Lincoln Memorial Circle Northwest: Keep right onto Lincoln Memorial Circle Northwest in 0.064 km
-: Arrive at Lincoln Memorial Circle Northwest, on the right in 0.000 km
```

OpenRouteService geocoding isn't perfect; in the latter example the correct
monuments would not have been found without specifying the monuments in D.C.

